import cs from '../dist/index';
import { expect } from 'chai';



describe('classNames 函数测试', () => {

  it(`('foo', 'bar')`, () => {
    expect(cs('foo', 'bar')).equal('foo bar');
  });

  it(`({'foo-bar': true})`, () => {
    expect(cs({'foo-bar': true})).equal('foo-bar');
  });

  it(`({'foo-bar': false})`, () => {
    expect(cs({'foo-bar': false})).equal('');
  });

  it(`({foo: true}, {bar: true})`, () => {
    expect(cs({foo: true}, {bar: true})).equal('foo bar');
  });

  it(`({foo: true, bar: true})`, () => {
    expect(cs({foo: true, bar: true})).equal('foo bar');
  });

  it(`('foo', { bar: true, duck: false }, 'baz', { quux: true })`, () => {
    expect(cs('foo', { bar: true, duck: false }, 'baz', { quux: true })).equal('foo bar baz quux');
  });

  it(`(null, false, 'bar', undefined, 0, 1, { baz: null }, '')`, () => {
    expect(cs(null, false, 'bar', undefined, 0, 1, { baz: null }, '')).equal('bar');
  });

  it(`array`, () => {
    var arr = ['b', { c: true, d: false }];
    expect(cs('a', arr)).equal('a b c');
  })
});
