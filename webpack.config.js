module.exports = {
  devtool: '#sourcemap',
  entry: {
    index: './test/tests.es6.js'
  },
  output: {
    path: __dirname,
    filename: './test/tests.js'
  },
  mode: 'development',
  // mode: 'production',
  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.json']
  }
}

