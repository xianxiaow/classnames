import { isString, isObject, isArray } from 'tiny-is';

const hasOwn = {}.hasOwnProperty;

export default function classNames(...args) {
  const classes = [];

  for (let i = 0; i < args.length; i++) {
    let arg = args[i];
    if (!arg) continue;

    if (isString(arg)) {
      classes.push(arg)
    } else if (isArray(arg) && arg.length) {
      const inner = classNames.apply(null, arg);
      if (inner) {
        classes.push(inner);
      }
    } else if (isObject(arg)) {
      for(let key in arg) {
        if (hasOwn.call(arg, key) && arg[key]) {
          classes.push(key);
        }
      }
    }
  }
  return classes.join(' ');
}
